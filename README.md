# Snips Calculator Skill

It is compatible with the format expected by the `snips-skill-server`  
This app uses Python 3  

## Setup

This app requires some python dependencies to work properly, these are
listed in the `requirements.txt`. You can use the `setup.sh` script to
create a python virtualenv that will be recognized by the skill server
and install them in it.

## Functions

Examples of how to use the app:  
"Hey Snips"..  
- Whats the square root of 25  
- How much is five plus 5 multiplied by 7  
- Calculate 2 to the power of 7  
- Nine plus nine equals how much  
- Two plus six times eight subtract ten multiplied by four  
