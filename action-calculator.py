#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### **************************************************************************** ###
#
# Project: Calculator Skill for Snips
# Created Date: Wednesday, January 30th 2019, 6:41:12 pm
# Author: Greg
# -----
# Last Modified: Fri May 10 2019
# Modified By: Greg
# -----
# Copyright (c) 2019 Greg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
# AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
### **************************************************************************** ###



from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from hermes_python.ontology import *
from Utilities import Utilities
import io

util = Utilities()


def strip_float_point_zero(f):
        """removes '.0' from the end of a float

        Arguments:
            text {float} -- float to remove '.0' from

        Returns:
            str -- striped string
        """
        f = repr(f)
        if not f.endswith(".0"):
            return f
        return f[:len(f)-len(".0")]


def subscribe_intent_mathsQuestion(hermes, intentMessage):
    sayMessage = "Something went wrong, I can not do that maths calculation for you"

    try:
        # uses BODMAS.. so in order do division and multiplication, then addition and subtraction..from left to right
        #slot_dict = intentMessage.slots.toDict()
        function_array = []
        number_array = []
        function_array = intentMessage.slots['function'].all()
        number_array = intentMessage.slots['number'].all()
            
        is_error = False

        mynumbers = []
        for num in number_array:
            mynumbers.append(num.value)

        myfunctions = []
        if not function_array is None:
            for fun in function_array:
                myfunctions.append(fun.value)

        
        funloopindex = 0
        evalString = ""
        for index,num in enumerate(mynumbers):
            mathsstr = "{}".format(num)
            if num < 0 :
                mathsstr = "+{}".format(num)
            
            functionstr = ""
            insertString = ""

            if len(myfunctions) > funloopindex:
                functionstr = "{}".format(myfunctions[funloopindex])

            if functionstr == "squared":
                insertString = "**2"
            elif functionstr == "cubed":
                insertString = "**3"
            elif functionstr == "square root":
                insertString = "**0.5"
            elif functionstr == "cubed root":
                insertString = "**0.3333333333"
                
            if len(insertString) > 0:
                funloopindex +=1
                functionstr = ""
                if len(myfunctions) > funloopindex:
                    functionstr = "{}".format(myfunctions[funloopindex])
            
            if functionstr == "plus":
                functionstr = "+"
            elif functionstr == "minus":
                functionstr = "-"
            elif functionstr == "times":
                functionstr = "*"
            elif functionstr == "to the power of":
                functionstr = "**"
            elif functionstr == "divide":
                # has to be "divide"
                # is the next number a ZERO.. cant divide by ZERO
                next_number = mynumbers[index+1]
                if next_number == 0:
                    sayMessage = "Sorry, I can not divide by zero"
                    is_error = True
                    break
                else:
                    functionstr = "/"

            evalString = "{}{}{}{}".format(evalString,mathsstr,insertString,functionstr)
            evalString = evalString.replace("++","+")
            funloopindex +=1

        
        if not is_error:
            answer = eval(evalString)
            answer = util.number_to_words(answer, 5)
            #answer = strip_end(p.number_to_words(float_to_str(answer,5))).strip()
            sayMessage = "That would be {}".format(answer)
        
    except Exception as e:
        print("Error in mathsQuestion Snippet: {}".format(e))
    
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, sayMessage)


if __name__ == "__main__":
    mqtt_opts = MqttOptions()
    with Hermes(mqtt_options=mqtt_opts) as h:
        h.subscribe_intent("ozie:MathsCalculations", subscribe_intent_mathsQuestion) \
            .loop_forever()